//
//  NewsDetailViewController.swift
//  HaberUygulamam
//
//  Created by Kerim Çağlar on 05/08/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    var newsDetail:String?
    @IBOutlet weak var detailText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailText.text = newsDetail
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
