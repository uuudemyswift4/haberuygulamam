//
//  ViewController.swift
//  HaberUygulamam
//
//  Created by Kerim Çağlar on 27/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import AEXML
import Alamofire
import SDWebImage

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var titles = [String]()
    var descriptions = [String]()
    var urls = [String]()
    var newsImageUrls = [String]()
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Alamofire.request("http://www.trt.net.tr/rss/cevre.rss").response { (response) in
            
            guard let xmlData = response.data else {return}
            
            let options = AEXMLOptions()
            
            do{
               let xmlDoc = try AEXMLDocument(xml: xmlData, options: options)
                //print(xmlDoc.root["channel"]["item"]["title"].value!)
                if let items = xmlDoc.root["channel"]["item"].all{
                    for item in items{
                        self.titles.append(item["title"].string)
                        self.descriptions.append(item["description"].string)
                        self.urls.append(item["link"].string)
                        let enclosure = item["enclosure"]
                        if let newsImageUrl = enclosure.attributes["url"]{
                            self.newsImageUrls.append(newsImageUrl)
                        }
                    }
                    self.myTableView.reloadData()
                }
            }catch{
                print("Hata oluştu")
            }
        }
    }
    
    // TABLEVIEW ISLEMLERI
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! NewsTableViewCell
        
        
        //let imageView = cell.viewWithTag(1) as! UIImageView
        //imageView.sd_setImage(with: URL(string: newsImageUrls[indexPath.row]))
        
        let _ = SDWebImageDownloader.shared().downloadImage(with: URL(string:newsImageUrls[indexPath.row]), options: [], progress: nil) { (image, data, error, finish) in
            
            DispatchQueue.main.async {
                cell.newsImage.image = image
            }
        }
        
        cell.title.text = titles[indexPath.row]
        cell.url.text = urls[indexPath.row]
        
        return cell
    }
    
    //Herhangi bir satıra tıklamanın algılanması
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        let newsDetailVC = storyboard?.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailViewController
        newsDetailVC.newsDetail = descriptions[indexPath.row]
        
        self.present(newsDetailVC, animated: true, completion: nil)
        
    }

}

