//
//  NewsTableViewCell.swift
//  HaberUygulamam
//
//  Created by Kerim Çağlar on 29/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
  
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var url: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
